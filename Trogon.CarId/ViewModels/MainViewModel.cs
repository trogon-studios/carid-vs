using System;
using System.Collections.Generic;
using System.Linq;
using Trogon.CarId.Helpers;
using Trogon.CarId.Models;

namespace Trogon.CarId.ViewModels
{
    public class MainViewModel : Observable
    {
        private IEnumerable<CountryDataGroup> countries;
        private CountryDataGroup country;
        private string licensePlate;
        private string provinceName;
        private string districtName;

        public MainViewModel()
        {
        }

        public async void Init()
        {
            Countries = await CountryDataSource.GetGroupsAsync();
            Country = countries.FirstOrDefault();
            await PlateDataSource.GetGroupsAsync();
        }

        public IEnumerable<CountryDataGroup> Countries
        {
            get { return countries; }
            private set { Set(ref countries, value); }
        }

        public CountryDataGroup Country
        {
            get { return country; }
            set { Set(ref country, value); }
        }

        public string LicensePlate
        {
            get { return licensePlate; }
            set { Set(ref licensePlate, value); FindPlateDetails(); }
        }

        public string ProvinceName
        {
            get { return provinceName; }
            set { Set(ref provinceName, value); }
        }

        public string DistrictName
        {
            get { return districtName; }
            set { Set(ref districtName, value); }
        }

        private async void FindPlateDetails()
        {
            if (LicensePlate != null)
            {
                ProvinceName = "nieznane";
                DistrictName = "nieznany";
                var key = LicensePlate.Trim();
                if (key.Length > 0)
                {
                    var provice = await PlateDataSource.GetGroupAsync(key.Substring(0, 1));
                    if (provice != null)
                    {
                        ProvinceName = provice.Name;
                        PlateDataItem district = null;
                        if (key.Length > 2)
                        {
                            district = await PlateDataSource.GetItemAsync(key.Substring(0, 3));
                        }
                        if (district == null && key.Length > 1)
                        {
                            district = await PlateDataSource.GetItemAsync(key.Substring(0, 2));
                        }
                        if (district != null)
                        {
                            DistrictName = district.Name;
                        }
                    }
                }
            }
        }
    }
}
