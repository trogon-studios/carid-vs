using Trogon.CarId.ViewModels;

using Windows.UI.Xaml.Controls;

namespace Trogon.CarId.Views
{
    public sealed partial class MainPage : Page
    {
        public MainViewModel ViewModel { get; } = new MainViewModel();
        public MainPage()
        {
            DataContext = ViewModel;
            InitializeComponent();
        }

        private void Page_Loaded(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            ViewModel.Init();
        }

        private void TextBox_KeyDown(object sender, Windows.UI.Xaml.Input.KeyRoutedEventArgs e)
        {
            var textBox = sender as TextBox;
            if (textBox != null
                && (int)e.Key >= (int)Windows.System.VirtualKey.A
                && (int)e.Key <= (int)Windows.System.VirtualKey.Z)
            {

                textBox.Text += e.Key.ToString();
                textBox.SelectionStart = textBox.Text.Length;
                textBox.SelectionLength = 0;

                e.Handled = true;
            }
        }
    }
}
