using System;

namespace Trogon.CarId.Services
{
    public class SuspensionState
    {
        public Object Data { get; set; }
        public DateTime SuspensionDate { get; set; }
    }
}
