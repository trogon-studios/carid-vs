﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using Windows.Data.Json;
using Windows.Storage;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;

// The data model defined by this file serves as a representative example of a strongly-typed
// model.  The property names chosen coincide with data bindings in the standard item templates.
//
// Applications may use this model as a starting point and build on it, or discard it entirely and
// replace it with something appropriate to their needs. If using this model, you might improve app 
// responsiveness by initiating the data loading task in the code behind for App.xaml when the app 
// is first launched.

namespace Trogon.CarId.Models
{
    /// <summary>
    /// Generic item data model.
    /// </summary>
    public class CountryDataItem
    {
        public CountryDataItem(String uniqueId, String name)
        {
            this.UniqueId = uniqueId;
            this.Name = name;
        }

        public string UniqueId { get; private set; }
        public string Name { get; private set; }

        public override string ToString()
        {
            return this.Name;
        }
    }

    /// <summary>
    /// Generic group data model.
    /// </summary>
    public class CountryDataGroup
    {
        public CountryDataGroup(String uniqueId, String name)
        {
            this.UniqueId = uniqueId;
            this.Name = name;
            this.Items = new ObservableCollection<CountryDataItem>();
        }

        public string UniqueId { get; private set; }
        public string Name { get; private set; }
        public ObservableCollection<CountryDataItem> Items { get; private set; }

        public override string ToString()
        {
            return this.Name;
        }
    }

    /// <summary>
    /// Creates a collection of groups and items with content read from a static json file.
    /// 
    /// PlateDataSource initializes with data read from a static json file included in the 
    /// project.  This provides sample data at both design-time and run-time.
    /// </summary>
    public sealed class CountryDataSource
    {
        private static CountryDataSource _CountryDataSource = new CountryDataSource();

        private ObservableCollection<CountryDataGroup> _groups = new ObservableCollection<CountryDataGroup>();
        public ObservableCollection<CountryDataGroup> Groups
        {
            get { return this._groups; }
        }

        public static async Task<IEnumerable<CountryDataGroup>> GetGroupsAsync()
        {
            await _CountryDataSource.GetCountryDataAsync();

            return _CountryDataSource.Groups;
        }

        public static async Task<CountryDataGroup> GetGroupAsync(string uniqueId)
        {
            await _CountryDataSource.GetCountryDataAsync();
            // Simple linear search is acceptable for small data sets
            var matches = _CountryDataSource.Groups.Where((group) => group.UniqueId.Equals(uniqueId));
            if (matches.Count() == 1)
                return matches.First();
            return null;
        }

        public static async Task<CountryDataItem> GetItemAsync(string uniqueId)
        {
            await _CountryDataSource.GetCountryDataAsync();
            // Simple linear search is acceptable for small data sets
            var matches = _CountryDataSource.Groups.SelectMany(group => group.Items).Where((item) => item.UniqueId.Equals(uniqueId));
            if (matches.Count() == 1)
                return matches.First();
            return null;
        }

        private async Task GetCountryDataAsync()
        {
            if (this._groups.Count != 0)
                return;

            Uri dataUri = new Uri("ms-appx:///Assets/CountryData.json");

            StorageFile file = await StorageFile.GetFileFromApplicationUriAsync(dataUri);
            string jsonText = await FileIO.ReadTextAsync(file);
            JsonObject jsonObject = JsonObject.Parse(jsonText);
            JsonArray jsonArray = jsonObject["Groups"].GetArray();

            foreach (JsonValue groupValue in jsonArray)
            {
                JsonObject groupObject = groupValue.GetObject();
                CountryDataGroup group = new CountryDataGroup(groupObject["UniqueId"].GetString(),
                                                            groupObject["Name"].GetString());

                foreach (JsonValue itemValue in groupObject["Items"].GetArray())
                {
                    JsonObject itemObject = itemValue.GetObject();
                    group.Items.Add(new CountryDataItem(itemObject["UniqueId"].GetString(),
                                                       itemObject["Name"].GetString()));
                }
                this.Groups.Add(group);
            }
        }
    }
}
